package com.incubyte.tddassessment;

import org.apache.logging.log4j.util.Strings;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@SpringBootApplication
public class StringCalculator {

	public static int add(String numbers) throws Exception {
		if(!numbers.isEmpty()) {
			StringJoiner negatives = new StringJoiner(",");
			StringBuilder errorMessage = new StringBuilder("negatives not allowed ");
			StringBuilder delimeter = getDelimeter("\n");

			if(numbers.startsWith("//")) {
				String newDelim = numbers.substring(numbers.indexOf("//")+2, numbers.indexOf("\\n"));
				delimeter.append(newDelim);
				numbers = numbers.replace("//"+newDelim+"\\n","");
			}

			Integer sum = calculateSum(numbers, negatives, delimeter);
			if(negatives.length() != 0 )
				throw new NumberFormatException(errorMessage.append(negatives).toString());
			return sum;
		}
		return 0;
	}

	//this method is modified to satisfy the last testcase
	private static Integer calculateSum(String numbers, StringJoiner negatives, StringBuilder delimeter) {
		int[] ints = Arrays.stream(numbers.split("[" + delimeter + "]"))
				.filter(Strings::isNotEmpty)
				.map(Integer::parseInt)
				.mapToInt(Integer::intValue)
				.toArray();
		return IntStream.range(0, ints.length)
				.filter(i -> i % 2 != 0)
				.map(i -> ints[i])
				.reduce(0, Integer::sum);
	}

	private static StringBuilder getDelimeter(String newDelimeter) {
		if(newDelimeter.isEmpty())
			return new StringBuilder(",");
		StringBuilder delim = new StringBuilder(",");
		delim.append(newDelimeter);
		return delim;
	}

}
