package com.incubyte.tddassessment;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
@RunWith(SpringRunner.class)
class StringCalculatorTests {

	StringCalculator stringCalculator = new StringCalculator();
	@Test
	void addShouldReturnZeroForEmptyString() throws Exception {
		int response = stringCalculator.add("");
		assertEquals(0,response);
	}

	/**
	 * since the input is same digit the sum will be same as input
	 * @param input
	 * @param res
	 */
	@ParameterizedTest
	@CsvSource(value = {
			"1;1",
			"2;2"
	},delimiter = ';')
	void addShouldReturnInputForSingleDigit(String input, int res) throws Exception {
		int response = stringCalculator.add(input);
		assertEquals(res,response);
	}

	@ParameterizedTest
	@CsvSource(value = {
			"1,2;3",
			"2,2;4",
			"2,2,3;7",
			"2,2,3,4,5,6;22"
	},delimiter = ';')
	void addShouldReturnSumOfNumbersInString(String input, int res) throws Exception {
		int response = stringCalculator.add(input);
		assertEquals(res,response);
	}

	@Test
	void addShouldAcceptNewLineAsADelimeter() throws Exception {
		int response = stringCalculator.add("1\n2,3");
		assertEquals(response,6);
	}

	@Test
	void addShouldAcceptDifferentDelimeters() throws Exception {
		int response = stringCalculator.add("//;\\n1;2");
		assertEquals(response,3);
	}

	@ParameterizedTest
	@CsvSource(value = {
			"1,2,-1;negatives not allowed -1",
			"1,-2,-1;negatives not allowed -2,-1",
	},delimiter = ';')
	void addShouldThrowErrorForStringsWithNegativeNumbers(String input, String msg) {
		Throwable exception = assertThrows(
				NumberFormatException.class,
				() -> { stringCalculator.add(input); }
		);
		assertEquals(msg, exception.getMessage());
	}

	@Test
	void addShouldAcceptMultipleNewDelimeters() throws Exception {
		int response = stringCalculator.add("//;*\\n1;*2");
		assertEquals(response,3);
	}

	@Test
	void addShouldSkipAlternativeIndexes() throws Exception {
		int response = stringCalculator.add("//;*\\n1;*2;*3;*4");
		assertEquals(6,response);
	}
}
